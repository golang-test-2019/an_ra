package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/golang-test-2019/an_ra/dt"
)

func OK(w http.ResponseWriter, result interface{}) {
	json.NewEncoder(w).Encode(dt.Response{
		Success: true,
		Result:  result,
	})
}

func Fail(w http.ResponseWriter, result interface{}) {
	json.NewEncoder(w).Encode(dt.Response{
		Result: result,
	})
}
