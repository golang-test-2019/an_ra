package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"strings"

	"bitbucket.org/golang-test-2019/an_ra/dt"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

const DefaultError = "unexpected error"

var DB *sql.DB

func init() {
	db, err := sql.Open("mysql", os.Getenv("DB_CONN_STRING"))
	if err != nil {
		log.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	DB = db
}

func main() {
	http.HandleFunc("/api/users/create", userCreateHandler)
	http.HandleFunc("/api/users/sign-in", signInHandler)
	http.HandleFunc("/api/users/recover-password", recoverPasswordHandler)

	http.HandleFunc("/api/users/profile", profileHandler)

	log.Fatal(http.ListenAndServe(os.Getenv("REST_BIND_ADDRESS"), nil))
}

func userCreateHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		Fail(w, "unexpected HTTP method")
		return
	}

	dec := json.NewDecoder(r.Body)

	var u dt.User
	if err := dec.Decode(&u); err != nil {
		Fail(w, DefaultError)
		return
	}

	if h, err := bcrypt.GenerateFromPassword([]byte(u.Password), 0); err == nil {
		u.Password = string(h)
	} else {
		Fail(w, DefaultError)
		return
	}

	stmt, err := DB.Prepare(`
			INSERT INTO users (Email, Password)
			VALUES (?, ?)`)
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	res, err := stmt.Exec(u.Email, u.Password)
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	uid, err := res.LastInsertId()
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	OK(w, uid)
}

func signInHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		Fail(w, "unexpected HTTP method")
		return
	}

	dec := json.NewDecoder(r.Body)

	var credentials dt.User
	if err := dec.Decode(&credentials); err != nil {
		Fail(w, DefaultError)
		return
	}

	stmt, err := DB.Prepare("SELECT Id, Password FROM users WHERE Email = ? LIMIT 1")
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	row := stmt.QueryRow(credentials.Email)

	var u dt.User

	err = row.Scan(&u.Id, &u.Password)
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(credentials.Password)); err != nil {
		Fail(w, DefaultError)
		return
	}

	OK(w, u.Id)
}

func profileHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		stmt, err := DB.Prepare("SELECT Id, Name, Address, Email, Telephone FROM users WHERE Id = ? LIMIT 1")
		if err != nil {
			Fail(w, DefaultError)
			return
		}

		Id := r.FormValue("Id")

		row := stmt.QueryRow(Id)

		var u dt.User

		err = row.Scan(&u.Id, &u.Name, &u.Address, &u.Email, &u.Telephone)
		if err != nil {
			Fail(w, DefaultError)
			return
		}

		OK(w, u)
		return
	} else if r.Method == http.MethodPost {
		log.Println("POST")
		dec := json.NewDecoder(r.Body)

		var u dt.User
		if err := dec.Decode(&u); err != nil {
			Fail(w, DefaultError)
			return
		}

		stmt, err := DB.Prepare(`
			UPDATE users
			SET Name = ?, Address = ?, Email = ?, Telephone = ?
			WHERE Id = ?
			LIMIT 1`)
		if err != nil {
			Fail(w, DefaultError)
			return
		}

		_, err = stmt.Exec(u.Name, u.Address, u.Email, u.Telephone, r.FormValue("Id"))
		if err != nil {
			Fail(w, DefaultError)
			return
		}

		OK(w, nil)
		return
	}

	Fail(w, "unexpected HTTP method")
}

func recoverPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		Fail(w, "unexpected HTTP method")
		return
	}

	dec := json.NewDecoder(r.Body)

	var credentials dt.User
	if err := dec.Decode(&credentials); err != nil {
		Fail(w, DefaultError)
		return
	}

	if strings.Count(credentials.Email, "@") > 1 || strings.Count(credentials.Email, "\r\n") > 0 {
		Fail(w, DefaultError)
		return
	}

	tx, err := DB.Begin()
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	stmt, err := tx.Prepare("SELECT Id, Password FROM users WHERE Email = ? LIMIT 1")
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	row := stmt.QueryRow(credentials.Email)

	var u dt.User

	if err := row.Scan(&u.Id, &u.Password); err != nil {
		Fail(w, "that email does not exist in our database")
		return
	}

	// TODO: generate randomly
	code := "12345"

	stmt, err = tx.Prepare("INSERT INTO password_recovery (Code, UserId) VALUES (?, ?)")
	if _, err := stmt.Exec(code, u.Id); err != nil {
		Fail(w, DefaultError)
		return
	}

	if err := tx.Commit(); err != nil {
		Fail(w, DefaultError)
		return
	}

	to := []string{credentials.Email}
	msg := []byte("To: " + credentials.Email + "\r\n" +
		"Subject: Golang Test 2019 - Password Recovery\r\n" +
		"\r\n" +
		"Please open the following link to recover your password:\r\n" +
		"http://" + os.Getenv("WEB_HOSTNAME") + "/recover-password?code=" + code + "\r\n")

	auth := smtp.PlainAuth("",
		os.Getenv("EMAIL_USER"),
		os.Getenv("EMAIL_PASSWORD"),
		os.Getenv("EMAIL_SERVER"))

	smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_SERVER_PORT"),
		auth,
		os.Getenv("EMAIL_USER"),
		to,
		msg)

	OK(w, nil)
}

// TODO: receive JSON object
func setNewPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		Fail(w, "unexpected HTTP method")
		return
	}

	tx, err := DB.Begin()
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	code := r.FormValue("Code")
	password := r.FormValue("Password")

	stmt, err := tx.Prepare("SELECT UserId FROM users WHERE Code = ? LIMIT 1")
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	var uid int

	row := stmt.QueryRow(code)
	if err := row.Scan(&uid); err != nil {
		Fail(w, DefaultError)
		return
	}

	if h, err := bcrypt.GenerateFromPassword([]byte(password), 0); err == nil {
		password = string(h)
	} else {
		Fail(w, DefaultError)
		return
	}

	stmt, err = tx.Prepare("UPDATE users SET Password = ? WHERE Id = ? LIMIT 1")
	if err != nil {
		Fail(w, DefaultError)
		return
	}

	if err := tx.Commit(); err != nil {
		Fail(w, DefaultError)
		return
	}

	OK(w, nil)
}
