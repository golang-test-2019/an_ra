package dt

type Response struct {
	Success bool
	Result  interface{}
}

type User struct {
	Id        int
	Name      string
	Address   string
	Email     string
	Telephone string
	Password  string
}
