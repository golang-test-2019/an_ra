package main

import (
	"net/http"
)

const SESSION_NAME = "session"

func GetUserId(r *http.Request) int {
	sess, err := SessionStore.Get(r, SESSION_NAME)
	if err != nil {
		return 0
	}

	uid, ok := sess.Values["Id"].(int)
	if !ok {
		return 0
	}

	return uid
}

func SetErrorMsg(w http.ResponseWriter, r *http.Request, msg string) {
	sess, _ := SessionStore.Get(r, SESSION_NAME)
	sess.Values["Error"] = msg
	sess.Save(r, w)
}

func GetErrorMsg(w http.ResponseWriter, r *http.Request) string {
	sess, err := SessionStore.Get(r, SESSION_NAME)
	if err != nil {
		return ""
	}

	msg, ok := sess.Values["Error"].(string)
	if !ok {
		return ""
	}

	sess.Values["Error"] = ""
	sess.Save(r, w)

	return msg
}

func CreateSession(userId int, w http.ResponseWriter, r *http.Request) error {
	sess, err := SessionStore.Get(r, SESSION_NAME)
	if err != nil {
		return err
	}

	sess.Options.HttpOnly = true
	sess.Values["Id"] = userId
	sess.Save(r, w)

	return nil
}

func DestroySession(w http.ResponseWriter, r *http.Request) {
	sess, _ := SessionStore.Get(r, SESSION_NAME)
	sess.Options.MaxAge = -1
	sess.Save(r, w)
}
