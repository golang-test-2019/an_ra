package main

import (
	"html/template"
	"log"
	"net/http"
	"os"

	"bitbucket.org/golang-test-2019/an_ra/dt"
	"github.com/gorilla/sessions"
)

var SessionStore = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

const DefaultError = "An unexpected error occurred"
const InvalidCredentialsError = "Wrong email and/or password"

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/sign-up", signUpHandler)
	http.HandleFunc("/sign-in", signInHandler)
	http.HandleFunc("/sign-out", signOutHandler)
	http.HandleFunc("/recover-password-email", recoverPasswordEmailHandler)
	http.HandleFunc("/recover-password", recoverPasswordHandler)
	http.HandleFunc("/profile", RequireAuth(profileHandler))
	http.HandleFunc("/profile-edit", RequireAuth(editProfileHandler))

	http.ListenAndServe(os.Getenv("WEB_BIND_ADDRESS"), nil)
}

func renderTmpl(w http.ResponseWriter, name string, data interface{}) {
	tmpl := template.Must(template.New(name).ParseGlob("views/*.html"))

	err := tmpl.Execute(w, data)
	if err != nil {
		log.Fatal(err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	data := map[string]string{
		"Error": GetErrorMsg(w, r),
	}

	renderTmpl(w, "index.html", data)
}

func signUpHandler(w http.ResponseWriter, r *http.Request) {
	u := dt.User{
		Email:    r.FormValue("Email"),
		Password: r.FormValue("Password"),
	}

	uid, err := CreateUser(u)
	if err != nil {
		SetErrorMsg(w, r, DefaultError)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if err := CreateSession(uid, w, r); err != nil {
		SetErrorMsg(w, r, DefaultError)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	http.Redirect(w, r, "/profile", http.StatusFound)
}

func signInHandler(w http.ResponseWriter, r *http.Request) {
	uid, err := SignIn(r.FormValue("Email"), r.FormValue("Password"))
	if err != nil {
		SetErrorMsg(w, r, InvalidCredentialsError)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	if err := CreateSession(uid, w, r); err != nil {
		SetErrorMsg(w, r, DefaultError)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	http.Redirect(w, r, "/profile", http.StatusFound)
}

func signOutHandler(w http.ResponseWriter, r *http.Request) {
	DestroySession(w, r)
	http.Redirect(w, r, "/", http.StatusFound)
}

func recoverPasswordEmailHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: implement
}

func recoverPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		if err := RecoverPassword(r.FormValue("Code"), r.FormValue("Password")); err != nil {
			data := map[string]string{
				"Code":  r.FormValue("Code"),
				"Error": DefaultError,
			}

			renderTmpl(w, "recover-password.html", data)
		}

		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	data := map[string]string{
		"Code": r.FormValue("Code"),
	}

	renderTmpl(w, "recover-password.html", data)
}

func profileHandler(w http.ResponseWriter, r *http.Request) {
	if usr, err := GetProfile(GetUserId(r)); err == nil {
		var data struct {
			*dt.User
			Error string
		}

		data.User = usr

		renderTmpl(w, "profile.html", data)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func editProfileHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		u := dt.User{
			Id:        GetUserId(r),
			Name:      r.FormValue("Name"),
			Address:   r.FormValue("Address"),
			Email:     r.FormValue("Email"),
			Telephone: r.FormValue("Telephone"),
		}

		if err := UpdateProfile(u); err != nil {
			SetErrorMsg(w, r, DefaultError)
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}

	if usr, err := GetProfile(GetUserId(r)); err == nil {
		var data struct {
			*dt.User
			Error string
		}

		data.User = usr

		renderTmpl(w, "profile-edit.html", data)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
