package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strconv"

	"bitbucket.org/golang-test-2019/an_ra/dt"
)

var API_HOST = os.Getenv("API_HOST")

func CreateUser(usr dt.User) (int, error) {
	m, _ := json.Marshal(usr)

	resp, err := http.Post(API_HOST+"/api/users/create", "application/json", bytes.NewBuffer(m))
	if err != nil {
		return 0, errors.New("could not create user")
	}

	var res struct {
		dt.Response
		Result int
	}

	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&res); err != nil || !res.Success {
		return 0, errors.New("could not create user")
	}

	return res.Result, nil
}

func SignIn(email, password string) (int, error) {
	m, _ := json.Marshal(dt.User{
		Email:    email,
		Password: password,
	})

	resp, err := http.Post(API_HOST+"/api/users/sign-in", "application/json", bytes.NewBuffer(m))
	if err != nil {
		return 0, errors.New("could not sign in")
	}

	var res struct {
		dt.Response
		Result int
	}

	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&res); err != nil || !res.Success {
		return 0, errors.New("could not sign in")
	}

	return res.Result, nil
}

func GetProfile(id int) (*dt.User, error) {
	resp, err := http.Get(API_HOST + "/api/users/profile?Id=" + strconv.Itoa(id))
	if err != nil {
		return nil, errors.New("could not retrieve the profile")
	}

	var res struct {
		dt.Response
		Result dt.User
	}

	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&res); err != nil || !res.Success {
		return nil, errors.New("could not retrieve the profile")
	}

	return &res.Result, nil
}

func UpdateProfile(usr dt.User) error {
	m, _ := json.Marshal(usr)

	resp, err := http.Post(API_HOST+"/api/users/profile?Id="+strconv.Itoa(usr.Id), "application/json", bytes.NewBuffer(m))
	if err != nil {
		return errors.New("could not update profile")
	}

	var res dt.Response

	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&res); err != nil || !res.Success {
		return errors.New("could not update profile")
	}

	return nil
}

func SendPasswordRecoveryEmail() {
	// TODO: implement
}

func RecoverPassword(code string, password string) error {
	// TODO: implement
	return nil
}
